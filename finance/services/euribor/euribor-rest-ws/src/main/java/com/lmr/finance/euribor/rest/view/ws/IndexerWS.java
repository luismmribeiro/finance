/**
 * 
 */
package com.lmr.finance.euribor.rest.view.ws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.lmr.finance.euribor.dto.IndexerDto;
import com.lmr.finance.euribor.service.IndexerService;

/**
 * 
 *
 * @author <a href="mailto:luismmribeiro@gmail.com">Luis Ribeiro</a>
 * @version 1.0, 19/03/2014
 */
@Stateless
@Path("/indexer")
public class IndexerWS {
	@Inject
	private IndexerService indexService;
	
	private final static String DATE_FORMAT = "yyyy-mm-dd";
	
	@GET
	@Path("{indexer}/{startDate}/{endDate}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<IndexerDto> getIndexerByTypeAndDate(@PathParam("indexer") String indexer,
												 @PathParam("startDate") String startDate,
												 @PathParam("endDate") String endDate) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		List<IndexerDto> list = this.indexService.getIndexerByTypeAndDate(indexer, dateFormat.parse(startDate), dateFormat.parse(endDate));
		
		return list;
	}
}
