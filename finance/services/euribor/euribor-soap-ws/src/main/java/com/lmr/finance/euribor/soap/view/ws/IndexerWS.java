/**
 * 
 */
package com.lmr.finance.euribor.soap.view.ws;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.lmr.finance.euribor.dto.IndexerDto;
import com.lmr.finance.euribor.service.IndexerService;

/**
 * 
 *
 * @author <a href="mailto:luismmribeiro@gmail.com">Luis Ribeiro</a>
 * @version 1.0, 19/03/2014
 */
@WebService
@Stateless
public class IndexerWS {
	@Inject
	private IndexerService indexService;
	
	@WebMethod
	public List<IndexerDto> getIndexerByTypeAndDate(String type, Date startDate, Date endDate) {
		return this.indexService.getIndexerByTypeAndDate(type, startDate, endDate);
	}
}
