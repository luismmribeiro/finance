/**
 * 
 */
package com.lmr.finance.euribor.dto;


/**
 *
 * @author <a href="mailto:luismmribeiro@gmail.com">Luis Ribeiro</a>
 * @version 1.0, 20/03/2014
 */
public class IndexerDto {
	private String indexer;
	private String date;
	private double value;
	
	public String getIndexer() {
		return indexer;
	}
	public void setIndexer(String indexer) {
		this.indexer = indexer;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}	
}
