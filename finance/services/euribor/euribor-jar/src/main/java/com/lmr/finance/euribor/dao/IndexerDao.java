/**
 * 
 */
package com.lmr.finance.euribor.dao;

import java.util.Date;
import java.util.List;

import com.ctp.cdi.query.Dao;
import com.ctp.cdi.query.EntityDao;
import com.lmr.finance.euribor.entity.Indexer;

/**
 *
 * @author <a href="mailto:luismmribeiro@gmail.com">Luis Ribeiro</a>
 * @version 1.0, 19/03/2014
 */
@Dao
public interface IndexerDao extends EntityDao<Indexer, Long> {

	List<Indexer> findByIndexerAndDateBetween(String indexerName, Date startDate, Date endDate);
}
