/**
 * 
 */
package com.lmr.finance.euribor.entity;

import java.util.Date;

import javax.persistence.Entity;

import org.hibernate.annotations.Index;

import com.lmr.finance.commons.entity.AbstractEntity;

/**
 * 
 *
 * @author <a href="mailto:luismmribeiro@gmail.com">Luis Ribeiro</a>
 * @version 1.0, 19/03/2014
 */
@Entity
public class Indexer extends AbstractEntity {
	private static final long serialVersionUID = 2526946363729540358L;
	
	// WARNING: Non standard (Hibernate only) annotation
	// Name: "Index_<table>_<field>
	@Index(name = "Index_Indexer_Indexer")
	private String indexer;
	
	private Date date;
	private double value;
	
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}
	/**
	 * @return the indexer
	 */
	public String getIndexer() {
		return indexer;
	}
	/**
	 * @param indexer the indexer to set
	 */
	public void setIndexer(String indexer) {
		this.indexer = indexer;
	}
}
