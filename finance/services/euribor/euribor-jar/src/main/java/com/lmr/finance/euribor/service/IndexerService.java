/**
 * 
 */
package com.lmr.finance.euribor.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

import com.lmr.finance.commons.service.AbstractEntityService;
import com.lmr.finance.euribor.dao.IndexerDao;
import com.lmr.finance.euribor.dto.IndexerDto;
import com.lmr.finance.euribor.entity.Indexer;

/**
 *
 * @author <a href="mailto:luismmribeiro@gmail.com">Luis Ribeiro</a>
 * @version 1.0, 19/03/2014
 */
public class IndexerService extends AbstractEntityService<Indexer, IndexerDao> {
	
	public List<IndexerDto> getIndexerByTypeAndDate(String type, Date startDate, Date endDate) {
		if(type == null || startDate == null || endDate == null) {
			throw new IllegalArgumentException("All the parameters must be provided (not null)!");
		}
		
		List<Indexer> list = this.getDao().findByIndexerAndDateBetween(type, startDate, endDate);
		List<IndexerDto> result = new ArrayList<>();
		
		Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
		
		for (Indexer indexer : list) {
			IndexerDto destObject = mapper.map(indexer, IndexerDto.class);
			result.add(destObject);
		}
		
		return result;
	}
}
