package com.lmr.finance.commons.service;

import java.util.List;

import javax.inject.Inject;

import com.ctp.cdi.query.EntityDao;
import com.lmr.finance.commons.entity.AbstractEntity;

/**
 * 
 * 
 * @author <a href="mailto:luis.ribeiro@glintt.com">Luis Ribeiro</a>
 * @version 1.0, 21/11/2013
 */
public abstract class AbstractEntityService<T extends AbstractEntity, D extends EntityDao<T, Long>> {

	@Inject
	private D dao;

	public void delete(T t) {
		getDao().remove(t);
	}

	public T save(T t) {
		return getDao().save(t);
	}

	protected D getDao() {
		return this.dao;
	}

	public List<T> findAll() {
		return getDao().findAll();
	}

	public Long count() {
		return getDao().count();
	}

	public T findBy(Long primaryKey) {
		return getDao().findBy(primaryKey);
	}
}
