package com.lmr.finance.commons.dao;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * 
 * @author <a href="mailto:luis.ribeiro@glintt.com">Luis Ribeiro</a>
 * @version 1.0, 25/10/2013
 */
@Stateless
public class EntityManagerProducer {
	@PersistenceContext
	@Produces
	private static EntityManager em;

	/**
	 * @return the em
	 */
	public static EntityManager getEm() {
		return em;
	}
}
