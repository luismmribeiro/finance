package com.lmr.finance.commons.entity.authorization;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import com.lmr.finance.commons.entity.AbstractEntity;

/**
 * 
 * 
 * @author <a href="mailto:luis.ribeiro@glintt.com">Luis Ribeiro</a>
 * @version 1.0, 21/11/2013
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "APP_USER")
public class User extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	@NotEmpty
	@Column(length = 128)
	private String firstName;

	@NotEmpty
	@Column(length = 128)
	private String lastName;

	@NotEmpty
	@Column(length = 128, unique = true)
	@Pattern(regexp = "^[\\w\\-]+(\\.[\\w\\-]+)*@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$")
	private String email;

	@NotEmpty
	@Column(length = 128, unique = true)
	private String username;

	@NotEmpty
	@Column(length = 128)
	private String password;

	@Column
	private boolean enabled;

	@Column
	private boolean firstLogin;

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(boolean firstLogin) {
		this.firstLogin = firstLogin;
	}
}
