/**
 * 
 */
package com.lmr.finance.commons.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @author <a href="mailto:luis.ribeiro@glintt.com">Luis Ribeiro</a>
 * @version 1.0, 17/07/2013
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

	private static final long serialVersionUID = -7859542477488691950L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	protected ToStringBuilder appendPropertiesToToString(
			ToStringBuilder toStringBuilder) {
		return toStringBuilder.append("id", this.getId());
	}
}
