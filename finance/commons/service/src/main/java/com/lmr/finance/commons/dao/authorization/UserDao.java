package com.lmr.finance.commons.dao.authorization;

import java.util.List;

import com.ctp.cdi.query.Dao;
import com.ctp.cdi.query.EntityDao;
import com.lmr.finance.commons.entity.authorization.User;

/**
 * 
 * 
 * @author <a href="mailto:luis.ribeiro@glintt.com">Luis Ribeiro</a>
 * @version 1.0, 23 de Out de 2013
 */
@Dao
public interface UserDao extends EntityDao<User, Long> {

	List<User> findByUsername(String username);

	User findByEmail(String email);
}
