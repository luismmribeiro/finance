/**
 * 
 */
package com.lmr.finance.commons.service.authorization;

import java.util.List;

import javax.inject.Inject;

import com.lmr.finance.commons.dao.authorization.UserDao;
import com.lmr.finance.commons.entity.authorization.User;
import com.lmr.finance.commons.service.AbstractEntityService;

/**
 * 
 * @author <a href="mailto:luis.ribeiro@glintt.com">Luis Ribeiro</a>
 * @version 1.0, 2 de Out de 2013
 */
public class UserService extends AbstractEntityService<User, UserDao> {

	public User findByUsername(String username) {
		List<User> userList = this.getDao().findByUsername(username);
		User result = null;
		if (!userList.isEmpty()) {
			// username is unique
			result = userList.get(0);
		}
		return result;
	}

	public User findByEmail(String email) {
		List<User> userList = this.getDao().findAll();
		for (User user : userList) {
			if (user.getEmail().equals(email)) {
				return user;
			}
		}
		return null;
	}
}